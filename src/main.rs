use std::collections::HashMap;

extern crate llvm_sys as llvm;
use llvm::prelude::*;
use std::ffi::CString;

use std::ptr;

mod grammar {
    #[derive(Debug)]
    pub enum Ast {
        AddExpr (Box<Ast>, Box<Ast>),
        SubExpr (Box<Ast>, Box<Ast>),
        MulExpr (Box<Ast>, Box<Ast>),
        DivExpr (Box<Ast>, Box<Ast>),
        PowExpr (Box<Ast>, Box<Ast>),
        IntExpr (i64),
        IfExpr (Box<Ast>, Box<Ast>, Box<Ast>),
        FnExpr (String, Vec<String>, Box<Ast>),
        CallExpr (String, Vec<Ast>),
    }

    include!(concat!(env!("OUT_DIR"), "/grammar.rs"));
}

use self::grammar::*;
use self::grammar::Ast::*;

fn populate_fn_map<'a>(asts: &'a Vec<Ast>)
    -> HashMap<&'a str, (&'a Vec<String>, &'a Ast)>
{
    let mut name_to_fns = HashMap::new();

    for ast in asts {
        inner(&ast, &mut name_to_fns);
    }

    fn inner<'a>(
        ast: &'a Ast, map: &mut HashMap<&'a str, (&'a Vec<String>, &'a Ast)>
    ) {
        match *ast {
            FnExpr (ref name, ref args, ref expr) => {
                map.insert(name, (args, expr));
            },
            _ => ()
        }
    }

    return name_to_fns;
}

fn eval_ast(ast: &Ast) -> i64 {
    return match *ast {
        IntExpr (x) => x,
        AddExpr (ref l, ref r) => eval_ast(l) + eval_ast(r),
        SubExpr (ref l, ref r) => eval_ast(l) - eval_ast(r),
        MulExpr (ref l, ref r) => eval_ast(l) * eval_ast(r),
        DivExpr (ref l, ref r) => eval_ast(l) / eval_ast(r),
        PowExpr (ref l, ref r) => eval_ast(l).pow(eval_ast(r) as u32),
        IfExpr (ref expr, ref truthy, ref falsey) => {
            let expr_result = eval_ast(expr);
            return if expr_result != 0 {
                eval_ast(truthy)
            }
            else {
                eval_ast(falsey)
            };
        },
        FnExpr (_, _, _) => 0,
        CallExpr (_, _) => 0,
    };
}

fn main() {
    println!("Hello, world!");
    println!("test_neg_assert");
    test_neg_assert();
    println!("test_pos_assert");
    test_pos_assert();
    println!("test_optional");
    test_optional();
    println!("test_list");
    test_list();
    println!("test_repeat");
    test_repeat();
    println!("test_borrowed");
    test_borrowed();
    println!("test_lifetime_parameter");
    test_lifetime_parameter();
    println!("test_boundaries");
    test_boundaries();
    println!("test_block");
    test_block();
    println!("test_keyval");
    test_keyval();
    println!("test_case_insensitive");
    test_case_insensitive();
    println!("test_position");
    test_position();
    println!("test_templates");
    test_templates();
    println!("test_neg_lookahead_err");
    test_neg_lookahead_err();

    // FIXME: Note that the current implementation of call-instruction
    // generation is dependent on the ordering of an iteration over the hashmap
    // of function definitions, so if the program segfaults, try swapping the
    // order of these two "fn" definitions.
    let fns_expr = r"
fn main ->
    myFunc()

fn myFunc ->
    3 + 4
";
    let fns_ast = funcs(fns_expr).unwrap();
    let name_to_fns = populate_fn_map(&fns_ast);
    println!("\nHashMap {:?}", name_to_fns);

    println!("------------------------------------------------");
    llvm_compile_fns(&name_to_fns);
    println!("------------------------------------------------");

    let arith_expr = "1+2*3-4+10";

    let ast2 = infix_arith_ast(arith_expr);
    println!("{:?}", ast2);
    let unwrapped_ast = ast2.unwrap();

    println!(
        "Val: {}",
        eval_ast(
            &unwrapped_ast
        )
    );

    println!(
        "Val: {}",
        eval_ast(
            &infix_arith_ast(arith_expr).unwrap()
        )
    );

    llvm_test();

    println!("\n");

    let arith_strings = vec![
        "1+2".to_owned(),
        "2*3".to_owned(),
        "10/2".to_owned(),
        "16+2".to_owned(),
    ];

    unsafe {
        llvm_arith(&arith_strings);
    }

    unsafe {
        let if_expr = "if 1 { 2 } else { 3 }";
        let if_ast = if_expr_ast(if_expr);
        let unwrapped_if_ast = if_ast.unwrap();

        println!("If eval: {}", eval_ast(&unwrapped_if_ast));
        llvm_compile(&unwrapped_if_ast);
    }
}

unsafe fn llvm_arith(arith_strings: &[String]) {
    let module = llvm::core::LLVMModuleCreateWithName(
        CString::new("arith_test").unwrap().as_ptr()
    );

    let ret_type = llvm::core::LLVMInt64Type();
    let function_type = llvm::core::LLVMFunctionType(
        ret_type, ptr::null_mut(), 0, 0
    );
    let function = llvm::core::LLVMAddFunction(
        module,
        CString::new("arith_fun").unwrap().as_ptr(),
        function_type
    );

    let builder = llvm::core::LLVMCreateBuilder();

    let mut vals = Vec::new();

    for arith_str in arith_strings {
        let bb = llvm::core::LLVMAppendBasicBlock(
            function, CString::new("calc_val").unwrap().as_ptr()
        );

        llvm::core::LLVMPositionBuilderAtEnd(builder, bb);

        let ast = infix_arith_ast(arith_str).unwrap();
        let arith_val = codegen_expr(module, function, builder, &ast);
        vals.push(arith_val);
    }

    {
        let bb = llvm::core::LLVMAppendBasicBlock(
            function, CString::new("last_block").unwrap().as_ptr()
        );
        llvm::core::LLVMPositionBuilderAtEnd(builder, bb);
    }

    let mut final_val = vals.pop().unwrap_or(
        llvm::core::LLVMConstInt(
            llvm::core::LLVMInt64Type(), 111, 0
        )
    );

    for next_val in vals {
        final_val = llvm::core::LLVMBuildAdd(
            builder, final_val, next_val,
            CString::new("final_val").unwrap().as_ptr()
        )
    }

    // Emit a ret of the arith expr
    llvm::core::LLVMBuildRet(builder, final_val);

    // Dump the module as IR to stdout.
    llvm::core::LLVMDumpModule(module);

    // Clean up
    llvm::core::LLVMDisposeBuilder(builder);
    llvm::core::LLVMDisposeModule(module);
}

fn llvm_compile_fns(fns: &HashMap<&str, (&Vec<String>, &Ast)>) {
    unsafe {
        let module = llvm::core::LLVMModuleCreateWithName(
            CString::new("llvm_compile").unwrap().as_ptr()
        );

        for (name, &(_, expr)) in fns {
            let ret_type = llvm::core::LLVMInt64Type();
            let function_type = llvm::core::LLVMFunctionType(
                ret_type, ptr::null_mut(), 0, 0
            );
            let function = llvm::core::LLVMAddFunction(
                module,
                CString::new(name.to_owned()).unwrap().as_ptr(),
                function_type
            );

            let builder = llvm::core::LLVMCreateBuilder();

            let bb = llvm::core::LLVMAppendBasicBlock(
                function, CString::new("entry").unwrap().as_ptr()
            );

            llvm::core::LLVMPositionBuilderAtEnd(builder, bb);

            let final_val = codegen_expr(module, function, builder, &expr);

            // Emit a ret of the arith expr
            llvm::core::LLVMBuildRet(builder, final_val);

            // Clean up
            llvm::core::LLVMDisposeBuilder(builder);
        }

        // Dump the module as IR to stdout.
        llvm::core::LLVMDumpModule(module);

        // Clean up
        llvm::core::LLVMDisposeModule(module);
    }
}

unsafe fn llvm_compile(expr: &Ast) {
    let module = llvm::core::LLVMModuleCreateWithName(
        CString::new("llvm_compile").unwrap().as_ptr()
    );

    let ret_type = llvm::core::LLVMInt64Type();
    let function_type = llvm::core::LLVMFunctionType(
        ret_type, ptr::null_mut(), 0, 0
    );
    let function = llvm::core::LLVMAddFunction(
        module,
        CString::new("main").unwrap().as_ptr(),
        function_type
    );

    let builder = llvm::core::LLVMCreateBuilder();

    let bb = llvm::core::LLVMAppendBasicBlock(
        function, CString::new("entry").unwrap().as_ptr()
    );

    llvm::core::LLVMPositionBuilderAtEnd(builder, bb);

    let final_val = codegen_expr(module, function, builder, &expr);

    // Emit a ret of the arith expr
    llvm::core::LLVMBuildRet(builder, final_val);

    // Dump the module as IR to stdout.
    llvm::core::LLVMDumpModule(module);

    // Clean up
    llvm::core::LLVMDisposeBuilder(builder);
    llvm::core::LLVMDisposeModule(module);
}

unsafe fn codegen_expr(
    module: LLVMModuleRef, function: LLVMValueRef, builder: LLVMBuilderRef,
    expr: &Ast
) -> LLVMValueRef {
    match *expr {
        IntExpr (x) => {
            llvm::core::LLVMConstInt(llvm::core::LLVMInt64Type(), x as u64, 0)
        },

        AddExpr (ref lhs, ref rhs) => {
            let lhs_u = codegen_expr(module, function, builder, lhs);
            let rhs_u = codegen_expr(module, function, builder, rhs);
            let name = CString::new("addtmp").unwrap();

            llvm::core::LLVMBuildAdd(builder, lhs_u, rhs_u, name.as_ptr())
        },

        SubExpr (ref lhs, ref rhs) => {
            let lhs_u = codegen_expr(module, function, builder, lhs);
            let rhs_u = codegen_expr(module, function, builder, rhs);
            let name = CString::new("subtmp").unwrap();

            llvm::core::LLVMBuildSub(builder, lhs_u, rhs_u, name.as_ptr())
        },

        MulExpr (ref lhs, ref rhs) => {
            let lhs_u = codegen_expr(module, function, builder, lhs);
            let rhs_u = codegen_expr(module, function, builder, rhs);
            let name = CString::new("multmp").unwrap();

            llvm::core::LLVMBuildMul(builder, lhs_u, rhs_u, name.as_ptr())
        },

        DivExpr (ref lhs, ref rhs) => {
            let lhs_u = codegen_expr(module, function, builder, lhs);
            let rhs_u = codegen_expr(module, function, builder, rhs);
            let name = CString::new("divtmp").unwrap();

            llvm::core::LLVMBuildSDiv(builder, lhs_u, rhs_u, name.as_ptr())
        },

        PowExpr (_, _) => {
            llvm::core::LLVMConstInt(llvm::core::LLVMInt64Type(), 0, 0)
        },

        IfExpr (ref expr, ref truthy, ref falsey) => {
            let expr_u = codegen_expr(module, function, builder, expr);
            let zero = llvm::core::LLVMConstInt(
                llvm::core::LLVMInt64Type(), 0, 0
            );
            let is_nonzero = llvm::core::LLVMBuildICmp(
                builder, llvm::LLVMIntPredicate::LLVMIntNE, expr_u, zero,
                CString::new("is_nonzero").unwrap().as_ptr()
            );

            let then_bb = llvm::core::LLVMAppendBasicBlock(
                function, CString::new("then").unwrap().as_ptr()
            );
            let else_bb = llvm::core::LLVMAppendBasicBlock(
                function, CString::new("else").unwrap().as_ptr()
            );
            let phi_bb = llvm::core::LLVMAppendBasicBlock(
                function, CString::new("phi").unwrap().as_ptr()
            );

            llvm::core::LLVMBuildCondBr(
                builder, is_nonzero, then_bb, else_bb
            );

            llvm::core::LLVMPositionBuilderAtEnd(builder, then_bb);

            let truthy_u = codegen_expr(module, function, builder, truthy);

            llvm::core::LLVMBuildBr(builder, phi_bb);

            llvm::core::LLVMPositionBuilderAtEnd(builder, else_bb);

            let falsey_u = codegen_expr(module, function, builder, falsey);

            llvm::core::LLVMBuildBr(builder, phi_bb);

            llvm::core::LLVMPositionBuilderAtEnd(builder, phi_bb);

            let phi = llvm::core::LLVMBuildPhi(
                builder, llvm::core::LLVMInt64Type(),
                CString::new("if_phi").unwrap().as_ptr()
            );

            let mut values = vec![truthy_u, falsey_u];
            let mut blocks = vec![then_bb, else_bb];

            llvm::core::LLVMAddIncoming(
                phi, values.as_mut_ptr(), blocks.as_mut_ptr(), 2
            );

            phi
        },

        FnExpr (_, _, _) => {
            llvm::core::LLVMConstInt(llvm::core::LLVMInt64Type(), 0, 0)
        },

        CallExpr (ref fn_name, _) => {
            let called = llvm::core::LLVMGetNamedFunction(
                module, CString::new(fn_name.to_owned()).unwrap().as_ptr()
            );

            let mut empty_args = Vec::<LLVMValueRef>::new();

            llvm::core::LLVMBuildCall(
                builder, called, empty_args.as_mut_ptr(), 0,
                CString::new("call_value").unwrap().as_ptr()
            )
        },
    }
}

fn llvm_test() {
    unsafe {
        // Set up a context, module and builder in that context.
        let context = llvm::core::LLVMContextCreate();
        let module = llvm::core::LLVMModuleCreateWithName(
            b"nop\0".as_ptr() as *const _
        );
        let builder = llvm::core::LLVMCreateBuilderInContext(context);

        // Get the type signature for void nop(void);
        // Then create it in our module.
        let void = llvm::core::LLVMVoidTypeInContext(context);
        let function_type = llvm::core::LLVMFunctionType(
            void, ptr::null_mut(), 0, 0
        );
        let function = llvm::core::LLVMAddFunction(
            module,
            b"nop\0".as_ptr() as *const _,
            function_type
        );

        // Create a basic block in the function and set our builder to generate
        // code in it.
        let bb = llvm::core::LLVMAppendBasicBlockInContext(
            context, function, b"entry\0".as_ptr() as *const _
        );
        llvm::core::LLVMPositionBuilderAtEnd(builder, bb);

        // Emit a `ret void` into the function
        llvm::core::LLVMBuildRetVoid(builder);

        // Dump the module as IR to stdout.
        llvm::core::LLVMDumpModule(module);

        // Clean up. Values created in the context mostly get cleaned up there.
        llvm::core::LLVMDisposeBuilder(builder);
        llvm::core::LLVMDisposeModule(module);
        llvm::core::LLVMContextDispose(context);
    }
}

fn test_neg_assert() {
    assert!(consonants("qwrty").is_ok());
    assert!(consonants("rust").is_err());
}

fn test_pos_assert() {
    assert_eq!(lookahead_result("abcd"), Ok("abc"));
    assert!(lookahead_result("abc").is_err());
}

fn test_optional() {
    assert_eq!(options("abc"), Ok(None));
    assert_eq!(options("abcdef"), Ok(Some(())));
    assert!(options("def").is_err());
}

fn test_list() {
    assert_eq!(list("5"), Ok(vec![5]));
    assert_eq!(list("1,2,3,4"), Ok(vec![1,2,3,4]));
}

fn test_repeat() {
    assert!(repeat_n("123").is_err());
    assert_eq!(repeat_n("1234"), Ok(vec![1,2,3,4]));
    assert!(repeat_n("12345").is_err());

    assert!(repeat_min("").is_err());
    assert!(repeat_min("1").is_err());
    assert_eq!(repeat_min("12"), Ok(vec![1,2]));
    assert_eq!(repeat_min("123"), Ok(vec![1,2,3]));

    assert_eq!(repeat_max(""), Ok(vec![]));
    assert_eq!(repeat_max("1"), Ok(vec![1]));
    assert_eq!(repeat_max("12"), Ok(vec![1,2]));
    assert!(repeat_max("123").is_err());

    assert!(repeat_min_max("").is_err());
    assert!(repeat_min_max("1").is_err());
    assert_eq!(repeat_min_max("12"), Ok(vec![1,2]));
    assert_eq!(repeat_min_max("123"), Ok(vec![1,2,3]));
    assert!(repeat_min_max("1234").is_err());

    assert!(repeat_sep_3("1,2").is_err());
    assert!(repeat_sep_3("1,2,3,4").is_err());
    assert_eq!(repeat_sep_3("1,2,3"), Ok(vec![1,2,3]));

    assert_eq!(repeat_variable("1a3abc222"), Ok(vec!["a", "abc", "22"]));
}

// before we were testing string matches using .slice(), which
// threw an ugly panic!() when we compared unequal character
// boundaries.. this popped up while parsing unicode
fn test_boundaries() {
    assert!(boundaries("f↙↙↙↙").is_err());
    assert!(case_insensitive("f↙↙↙↙").is_err());
}

fn test_borrowed() {
    assert_eq!(borrowed("abcd"), Ok("abcd"));
}

fn test_lifetime_parameter() {
    assert_eq!(&*lifetime_parameter("abcd").unwrap(), "abcd");
    assert_eq!(&*lifetime_parameter("COW").unwrap(), "cow");
}

fn test_block() {
    assert_eq!(block("foo"), Ok("foo"));
}

fn test_keyval() {
    let mut expected = HashMap::new();
    expected.insert(1, 3);
    expected.insert(2, 4);
    assert_eq!(keyvals("1:3\n2:4"), Ok(expected));
}

fn test_case_insensitive() {
    assert_eq!(case_insensitive("foo").unwrap(), "foo");
    assert_eq!(case_insensitive("FoO").unwrap(), "FoO");
    assert_eq!(case_insensitive("fOo").unwrap(), "fOo");
    assert_eq!(case_insensitive("FOO").unwrap(), "FOO");
    assert!(case_insensitive("boo").is_err());
    assert!(case_insensitive(" foo").is_err());
    assert!(case_insensitive("foo ").is_err());
}

fn test_position() {
    assert_eq!(position("aaaabbb").unwrap(), (0, 4, 7));
}

fn test_templates() {
    assert_eq!(parens("(asdf)").unwrap(), "asdf");
    assert_eq!(double_parens("((asdf))").unwrap(), "asdf");
}

fn test_neg_lookahead_err() {
    let err = neg_lookahead_err("ac").err().unwrap();
    assert_eq!(err.expected.len(), 1, "expected set includes: {:?}", err.expected);
    assert_eq!(err.offset, 1);
}
